import Foundation
import FileManager
import Model
import ModelExtentions

///This struct represents the game event methode
struct GameMain{
    var game : Game
    
    func gameStarts(board : Board){
        print(board)
        print("""
        **************************************
                ==>> GAME STARTS! <<==
        **************************************
        """)
    }
    
    func nextPlayer(player : Player ,board : Board){
        print("""
        **************************************
        \(player.id) - \(player.name), it's your turn!
        **************************************
        """)
    }
    
    func boardChanged(board : Board){
        print(board)
    }
    
    func resultsPublished(results : Result ,board : Board){
        switch results {
        case .notFinished:
            print("Game is not over yet!")
        case .winner(let owner, let reason):
            print("""
            **************************************
            Game Over!!!
            and the winner is... player\(owner.description)!
            \(reason.description).
            **************************************
            """)
        case .even:
            break
            
        }
    }
    
    func moveChanged(){
        print("Invalid Move")
    }
    
    func moveHasBeenChoosen(player : Player, move :  Move) {
        print("\(player.name) moved a piece \(move.description)")
    }
    
    func gameSave(gameToSave : Game) {
        let fileManger : MyFileManager = MyFileManager(ofTheProject: "DouShouQi", in: "tes.json")
        do{
            let data = try JSONEncoder().encode(gameToSave)
            try fileManger.writeData(of: data)
        }catch{
            print("Somthing wrong happend while saving")
        }
    }
    
    
  
    
    init(withRules Rules : Rules,andPlayer1 Player1: Player, andPlayer2 Player2: Player) throws{
        try game = Game(withRules: Rules, andPlayer1: Player1, andPlayer2: Player2)
        game.addListener(.gameStarted(gameStarts))
        game.addListener(.nextPlayerTurn(nextPlayer))
        game.addListener(.gameResultPublished(resultsPublished))
        game.addListener(.boardChanged(boardChanged))
        game.addListener(.invalidMoveEntered(moveChanged))
        game.addListener(.moveHasBeenChoosen(moveHasBeenChoosen))
        game.addListener(.saveGame(gameSave))
    }
    
    init(savedGame : Game) throws{
        game = savedGame
        game.addListener(.gameStarted(gameStarts))
        game.addListener(.nextPlayerTurn(nextPlayer))
        game.addListener(.gameResultPublished(resultsPublished))
        game.addListener(.boardChanged(boardChanged))
        game.addListener(.invalidMoveEntered(moveChanged))
        game.addListener(.moveHasBeenChoosen(moveHasBeenChoosen))
    }
    
    
    mutating func start() throws{
        try game.start()
    }
}
