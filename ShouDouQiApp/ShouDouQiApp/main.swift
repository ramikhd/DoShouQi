import Foundation
import Model
import ModelExtentions
import FileManager
import Persistance
/// This app is to test that the Model and ModelExtentions work
/// To test this I decided to create the Grid of a DouShouQi game
func testBoard(){
    let lion1 : Cell = Cell(ofType: .jungle,
                            piece: Piece(withOwner: .player1,
                                         andAnimal: .lion))
    
    let rat1 : Cell = Cell(ofType: .jungle,
                           piece: Piece(withOwner: .player1,
                                        andAnimal: .rat))
    
    let dog1 : Cell = Cell(ofType: .jungle,
                           piece: Piece(withOwner: .player1,
                                        andAnimal: .dog))
    
    let cat1 : Cell = Cell(ofType: .jungle,
                           piece: Piece(withOwner: .player1,
                                        andAnimal: .cat))
    
    let elephant1 : Cell = Cell(ofType: .jungle,
                                piece: Piece(withOwner: .player1,
                                             andAnimal: .elephant))
    
    let wolf1 : Cell = Cell(ofType: .jungle,
                            piece: Piece(withOwner: .player1,
                                         andAnimal: .wolf))
    
    let leopard1 : Cell = Cell(ofType: .jungle,
                               piece: Piece(withOwner: .player1,
                                            andAnimal: .leopard))
    
    let tiger1 : Cell = Cell(ofType: .jungle,
                             piece: Piece(withOwner: .player1,
                                          andAnimal: .tiger))
    
    let lion2 : Cell = Cell(ofType: .jungle,
                            piece: Piece(withOwner: .player2,
                                         andAnimal: .lion))
    
    let rat2 : Cell = Cell(ofType: .jungle,
                           piece: Piece(withOwner: .player2,
                                        andAnimal: .rat))
    
    let dog2 : Cell = Cell(ofType: .jungle,
                           piece: Piece(withOwner: .player2,
                                        andAnimal: .dog))
    
    let cat2 : Cell = Cell(ofType: .jungle,
                           piece: Piece(withOwner: .player2,
                                        andAnimal: .cat))
    
    let elephant2 : Cell = Cell(ofType: .jungle,
                                piece: Piece(withOwner: .player2,
                                             andAnimal: .elephant))
    
    let wolf2 : Cell = Cell(ofType: .jungle,
                            piece: Piece(withOwner: .player2,
                                         andAnimal: .wolf))
    
    let leopard2 : Cell = Cell(ofType: .jungle,
                               piece: Piece(withOwner: .player2,
                                            andAnimal: .leopard))
    
    let tiger2 : Cell = Cell(ofType: .jungle,
                             piece: Piece(withOwner: .player2,
                                          andAnimal: .tiger))
    
    
    let jungle : Cell = Cell(ofType: .jungle,
                             piece: Piece(withOwner: .noOne,
                                          andAnimal: .dog))
    
    let water : Cell = Cell(ofType: .water,
                            piece: Piece(withOwner: .noOne,
                                         andAnimal: .dog))
    
    let den : Cell = Cell(ofType: .den,
                          piece: Piece(withOwner: .noOne,
                                       andAnimal: .dog))
    
    let trap : Cell = Cell(ofType: .trap,
                           piece: Piece(withOwner: .noOne,
                                        andAnimal: .dog))
    
    let finalList : [[Cell]] = [
        [lion1,jungle,trap,den,trap,jungle,tiger1],
        [jungle,dog1,jungle,trap,jungle,cat1,jungle],
        [rat1,jungle,leopard1,jungle,wolf1,jungle,elephant1],
        [jungle,water,water,jungle,water,water,jungle],
        [jungle,water,water,jungle,water,water,jungle],
        [jungle,water,water,jungle,water,water,jungle],
        [jungle,water,water,jungle,water,water,jungle],
        [elephant2,jungle,wolf2,jungle,leopard2,jungle,rat2],
        [jungle, cat2,jungle,trap,jungle,dog2,jungle],
        [tiger2 ,jungle,trap,den,trap,jungle,lion2],
        
        
        
    ]
    
    var board : Board? = Board(withGrid: finalList)
    
    
    print("\(board!)")
    print("\(board!.countPieces())")
    print("\(board!.countPieces().player1)")
    
    print("\(board!.countPieces(of: .player1))")
    
    var res = board!.insert(piece: Piece(withOwner: .player1, andAnimal: .dog), atRow: 4, andColumn: 0)
    print("\n")
    print("\(res)")
    
    res = board!.insert(piece: Piece(withOwner: .player1, andAnimal: .dog), atRow: 100, andColumn: 0)
    print("\n")
    print("\(res)")
    
    res = board!.insert(piece: Piece(withOwner: .player1, andAnimal: .dog), atRow: -100, andColumn: 0)
    print("\n")
    print("\(res)")
    
    res = board!.insert(piece: Piece(withOwner: .player1, andAnimal: .dog), atRow: 0, andColumn: 0)
    print("\n")
    print("\(res)")
    
    print("\(board!)")
    
    res = board!.remove(atRow: 4, andColumn: 0)
    print("\n")
    print("\(res)")
    
    res = board!.remove(atRow: 100, andColumn: 0)
    print("\n")
    print("\(res)")
    
    res = board!.remove(atRow: 1, andColumn: 0)
    print("\n")
    print("\(res)")
    
    res = board!.remove(atRow: -100, andColumn: 0)
    print("\n")
    print("\(res)")
    
    print("\(board!)")
    
}

func makeMove(player: Player?, onBoard board: inout Board, withRules rules: Rules) -> Bool {
    guard let player else {
        return false
    }
    
    var move = player.chooseMove(in: board, with: rules)
    
    while move == nil {
        print("Invalid move")
        move = player.chooseMove(in: board, with: rules)
    }
    guard let move else{
        return false
    }
    
    board.remove(atRow: move.rowDestination, andColumn: move.columnDestination)
    board.insert(piece: board.grid[move.rowOrigin][move.columnOrigin].piece, atRow: move.rowDestination, andColumn: move.columnDestination)
    board.remove(atRow: move.rowOrigin, andColumn: move.columnOrigin)
            
    
    let gameOverResult = rules.isGameOver(fromBoard: board,
                                          atRow: move.rowDestination,
                                          andColumn: move.columnDestination)
    
    print(board)
    return gameOverResult.isWinner
}

func testRandomPlayer(player1: Player?, player2: Player?) {
    var board = VerySimpleRules.createBoard()
    let rules = VerySimpleRules()
    var isGameOver = false

    print(board)
    
    while !isGameOver {
        isGameOver = makeMove(player: player1, onBoard: &board, withRules: rules)
        if isGameOver {
            break
        }

        isGameOver = makeMove(player: player2, onBoard: &board, withRules: rules)
    }
}

func input(humanPlayer : HumanPlayer) -> Move?{
   print("Enter your move (rowOrigin columnOrigin rowDestination columnDestination):")
           
    if let input = readLine(), let move = parseMove(from: input, withHumanPlayer: humanPlayer) {
       return move
   } else {
       print("Invalid input. Try again.")
       return input(humanPlayer: humanPlayer)
   }
}


func inputPlayerName() -> String{
   print("Insert Player Name")
           
    if let input = readLine() {
       return input
   } else {
       print("Invalid input. Try again.")
       return inputPlayerName()
   }
}

func inputPlayer(of owner : Owner) -> Player?{
    print("Choose Player Type (ex: 1) :")
   print("""
           \t\t1-Human Player
           \t\t2-Dificult Bot
           \t\t3-Ez Bot
           """)
           
    if let inputChoice = readLine(), let choice = parsChoice(from: inputChoice)  {
        let name = inputPlayerName()
        if choice == 1 {
            return HumanPlayer(withName: name, andId: owner, andInputMethod: input)
        }else if choice == 2{
            return IAPlayer(withName: name, andId: owner)
        }else if choice == 3{
            return RandomPlayer(withName: name, andId: owner)
        }else{
            print("Invalid input \(choice) is not an option. Try again.")
            return inputPlayer(of: owner)
        }
   } else {
       print("Invalid input. Try again.")
       return inputPlayer(of: owner)
   }
}

func parsChoice(from input: String) -> Int? {
    return Int(input)
}


func parseMove(from input: String,withHumanPlayer humanPlayer : HumanPlayer) -> Move? {
       let components = input.split(separator: " ").compactMap { Int($0) }
       
       guard components.count == 4 else {
           return nil
       }
       
    return Move(owner: humanPlayer.id, rowOrigin: components[0],
                   columnOrigin: components[1],
                   rowDestination: components[2],
                   columnDestination: components[3])
}




func askForSavedGame() -> Bool {
    print("Do you want to start from a saved game? (yes/no)")
    
    if let input = readLine()?.lowercased() {
        if input == "yes" {
            return true
        } else if input == "no" {
            return false
        } else {
            print("Invalid input. Please enter 'yes' or 'no'.")
            return askForSavedGame()
        }
    } else {
        print("Invalid input. Please enter 'yes' or 'no'.")
        return askForSavedGame()
    }
}

var startFromSavedGame = askForSavedGame()
var gameMain : GameMain

if startFromSavedGame {
    let fileManger : MyFileManager = MyFileManager(ofTheProject: "DouShouQi", in: "tes.json")
    let gameData = try fileManger.readData()
    let configuration = GameConf(withInput: input)
    var game = try JSONDecoder().decode(Game.self, from: gameData!, configuration: configuration )
    gameMain = try GameMain(savedGame: game)
    
} else {
    
    var player1 : Player? = inputPlayer(of: .player1)
    var player2 : Player? = inputPlayer(of: .player2)
    
    //testRandomPlayer(player1: player1,player2: player2)
    
    //testRandomPlayer(player1: player1,player2: player2)
    
    let rules = VerySimpleRules()
    gameMain = try GameMain(withRules: rules, andPlayer1: player1!, andPlayer2: player2!)
    
}

try gameMain.start()


//let fileManger : MyFileManager = MyFileManager(ofTheProject: "DouShouQi", in: "tes.json")
//let data = try JSONEncoder().encode(VerySimpleRules.createBoard())
//try fileManger.writeData(of: data)
//
//let data1 = try fileManger.readData()
//let board = try JSONDecoder().decode(Board.self, from: data1!)
////let data2 = try JSONDecoder().decode(Board.self, from: <#T##Data#>)
//print("\(board)")
