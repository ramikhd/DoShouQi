import Foundation
import Model

///This extention make the visual of the class easier by using emoji
extension Cell{
    public var symbol: String { "\(cellType.symbol)\(piece?.owner != .noOne && piece?.owner != nil ? (piece?.animal.symbol ?? " ") : "   "  )\(piece?.owner.symbol ?? " ") " }
}
