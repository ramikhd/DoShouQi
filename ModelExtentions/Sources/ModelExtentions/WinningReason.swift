import Foundation
import Model

extension WinningReason{
    public var description: String {
        switch self {
        case .denReached:
                return "A den was reached"
        case .noMorePieces:
                return "A player has no more pieces left"
        case .noMovesLeft:
                return "A player has no more moves left"
        case .tooManyOccurrences:
                return "Repeted a pattern"
        case .unknown:
            return "Only Allah all and mighty knows"
        }
    }
}
