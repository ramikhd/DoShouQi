import Foundation
import Model

///This extention make the visual of the class easier by using emoji
extension Board : CustomStringConvertible{
    public var description: String {
        let delimiter : String = "\n"
        var description = ""
        for row in self.grid{
            for cell in row{
                description  += "\(cell.symbol)"
            }
            description+=delimiter
        }
        return description
    }
}
