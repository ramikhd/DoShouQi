import Foundation
import Model

///This extention make the visual of the class easier by using emoji
extension Owner {
    var symbol: String {
        switch self {
            case .noOne:
                return " "
            case .player1:
                return "🟡"
            case .player2:
                return "🔴"
        }
    }
}
