import Foundation
import Model

///This extention make the visual of the class easier by using emoji
extension CellType {
    var symbol: String {
        switch self {
            case .unknown:
                return " "
            case .jungle:
                return "🌿"
            case .den:
                return "🪹"
            case .trap:
                return "🪤"
            case .water:
                return "💧"
        }
    }
}
