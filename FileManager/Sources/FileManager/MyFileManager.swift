import Foundation
@available(macOS 13.0, *)
public struct MyFileManager{
    private let FileName : String
    private let Project : String
    
    public init(ofTheProject project : String,in fileName : String){
        FileName = fileName
        Project = project
    }
    
    public func getSaveDirectoryUrl() throws -> URL {
        let baseUrl = try URL(for: .documentDirectory, in: .userDomainMask).appending(path: Project)
        if !FileManager.default.fileExists(atPath: baseUrl.path) {
            try FileManager.default.createDirectory(at: baseUrl, withIntermediateDirectories: true)
        }
        let filePath = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appending(path: Project).appending(path: FileName)
        
        if !FileManager.default.fileExists(atPath: filePath.absoluteString) {
             FileManager.default.createFile(atPath: filePath.absoluteString, contents: nil)
        }
        
        return filePath
    }
    
    public func writeData(of data: Data) throws {
        do {
            try data.write(to: try getSaveDirectoryUrl())
        } catch {
            print("Error saving data: \(error)")
        }
    }

    
    public func readData() throws -> Data? {
        do {
            return try Data(contentsOf: try getSaveDirectoryUrl())
        } catch {
            print("Error loading data: \(error)")
        }
        return nil
    }
}
