# DouShouQi
## Introduction
This is a university project where the. objective is to code the DouShouQi [more information](https://fr.wikipedia.org/wiki/Jeu_du_combat_des_animaux) in a console app using swift

### The Model package
All this is in a package called DouShouQi
```mermaid
classDiagram
direction LR

	class Rules {
	    <<protocol>>
	    +createBoard()$ Board
	    +checkBoard(b: Board)$
	    +getNextPlayer() Owner
	    +getMoves(Board, Owner) Array~Move~
	    +getMoves(Board, Owner, Int, Int) Array~Move~
	    +isMoveValid(Board, Int, Int, Int, Int) Bool
	    +isMoveValid(Board, Move) Bool
	    +isGameOver(Board, Int, Int) : (Bool, Result)  
	    +playedMove(Move, Board, Board)
	    +occurences : [Board:Int]
	    +historic: [Move]
	}
	Rules <|.. ClassicRules
	Rules <|.. VerySimpleRules
	
	class Move {
	    <<struct>>
	    +owner: Owner
	    +rowOrigin: Int
	    +columnOrigin: Int
	    +rowDestination: Int
	    +columnDestination: Int
	}
	
	class Result {
	    <<enum>>
	    notFinished
	    even
	    winner(Owner, WinningReason)
	}
	class WinningReason {
	    <<enum>>
	    unknown
	    denReached
	    noMorePieces
	    tooManyOccurences
	    noMovesLeft
	}
	Result ..> WinningReason
	Rules ..> Move
	Rules ..> Result
	Rules ..> Board
	
	class InvalidBoardError {
	    <<enum>>
	    badDimensions(Int, Int)
	    badCellType(CellType,Int,Int)
	    multipleOccurencesOfSamePiece(Piece)
	    pieceWithNoOwner(Piece)
	    pieceNotAllowedOnThisCell(Piece, Cell)
	}
	
	ClassicRules ..> InvalidBoardError
	VerySimpleRules ..> InvalidBoardError
	
	class GameError {
	    <<enum>>
	    invalidMove
	}
	
	ClassicRules ..> GameError
	VerySimpleRules ..> GameError
class Board {
    <<struct>>
    +nbRows : Int
    +nbColumns : Int
    +init?(withGrid:)
}

class Cell {
    <<struct>>
    +init(ofType:ownedBy:withPiece:)
}

class CellType {
    <<enum>>
    unknown
    jungle
    water
    trap
    den
}

class Owner {
    noOne
    player1
    player2
}

Cell --> "1" CellType : cellType
Cell --> "1" Owner : initialOwner
Board --> "*" Cell : grid

class Animal {
    <<enum>>
    rat
    cat
    dog
    wolf
    leopard
    tiger
    lion
    elephant
}

class Piece {
    <<struct>>
    +init(withOwner:andAnimal:)
}

class BoardResult {
  <<enum>>
  unknown
  ok
  failed(reason:)
}

class BoardFailingReason {
  <<enum>>
  unknown
  outOfBounds
  cellNotEmpty
  cellEmpty
}

class Player {
    + id: Owner
    + name: String 
    + init?(withName: String, andId: Owner)
    + chooseMove(in: Board, with: Rules) : Move?
}

class RandomPlayer {
    +init?(withName: String, andId: Owner)
    +chooseMove(in: Board, with: Rules) : Move?
}

class IAPlayer {
    +init?(withName: String, andId: Owner)
    +chooseMove(in: Board, with: Rules) : Move?
}

class HumanPlayer {
    +init?(withName: String, andId : Owner, andInputMethod: (HumanPlayer) -> Move)
    +input : (HumanPlayer) -> Move?
    +chooseMove(in: Board, with: Rules) : Move?
}

Player <|-- RandomPlayer
Player <|-- HumanPlayer
Player <|-- IAPlayer

Player ..> Rules
Player ..> Board
Player ..> Owner
Board ..> BoardResult
BoardResult ..> BoardFailingReason
Piece --> "1" Owner : owner
Piece --> "1" Animal : animal
Cell --> "?" Piece : piece

class Game {
    <<struct>>
    +init(withRules: Rules, andPlayer1: Player, andPlayer2: Player)
    +start()
}

Game --> "1" Rules : rules
Game --> "1" Board : board
Game --> "2" Player: players

class GameError {
    <<enum>>
    nextPlayerError
    badPlayerId(String)
}

Game ..> GameError

```

### The ModelExtentions package

This package aime to make the visual represent in the command line tool of the struct and enum to be prettier by adding a calculated property called Symbol or the methode description if it is not defined in Model package

### The tests

I tested my class by reconstruct the orignal grid of the game DouShouQi. (functionel tests)
```
    🌿🦁🟡  🌿      🪤      🪹      🪤      🌿      🌿🐯🟡  

    🌿     🌿🐶🟡   🌿      🪤      🌿      🌿🐱🟡   🌿      

    🌿🐭🟡  🌿      🌿🐆🟡   🌿      🌿🐺🟡   🌿       🌿🐘🟡  

    🌿      💧      💧      🌿      💧      💧      🌿      

    🌿      💧      💧      🌿      💧      💧      🌿      

    🌿      💧      💧      🌿      💧      💧      🌿      

    🌿      💧      💧      🌿      💧      💧      🌿      

    🌿🐘🔴   🌿      🌿🐺🔴  🌿      🌿🐆🔴   🌿      🌿🐭🔴  

    🌿      🌿🐱🔴   🌿      🪤      🌿      🌿🐶🔴  🌿      

    🌿🐯🔴   🌿      🪤      🪹      🪤      🌿      🌿🦁🔴  
  ```

To launch the project(CLT) choose the project in Xcode and click on the play button or command+R

To launch the test choose the test in Xcode and command+U 

  # ;) ENJOY

  Also there is Unit test for the struct Board that test its methodes 



  To launch the project choose the project in Xcode and click on the play button ;) ENJOY
