import XCTest
@testable import Model

///These tests aim to test the Board struct
final class BoardTests: XCTestCase {
    var board : Board!
    
    override func setUp() {
        let lion1 : Cell = Cell(ofType: .jungle,
                                piece: Piece(withOwner: .player1,
                                             andAnimal: .lion))
        
        let rat1 : Cell = Cell(ofType: .jungle,
                               piece: Piece(withOwner: .player1,
                                            andAnimal: .rat))
        
        let dog1 : Cell = Cell(ofType: .jungle,
                               piece: Piece(withOwner: .player1,
                                            andAnimal: .dog))
        
        let cat1 : Cell = Cell(ofType: .jungle,
                               piece: Piece(withOwner: .player1,
                                            andAnimal: .cat))
        
        let elephant1 : Cell = Cell(ofType: .jungle,
                                    piece: Piece(withOwner: .player1,
                                                 andAnimal: .elephant))
        
        let wolf1 : Cell = Cell(ofType: .jungle,
                                piece: Piece(withOwner: .player1,
                                             andAnimal: .wolf))
        
        let leopard1 : Cell = Cell(ofType: .jungle,
                                   piece: Piece(withOwner: .player1,
                                                andAnimal: .leopard))
        
        let tiger1 : Cell = Cell(ofType: .jungle,
                                 piece: Piece(withOwner: .player1,
                                              andAnimal: .tiger))
        
        let lion2 : Cell = Cell(ofType: .jungle,
                                piece: Piece(withOwner: .player2,
                                             andAnimal: .lion))
        
        let rat2 : Cell = Cell(ofType: .jungle,
                               piece: Piece(withOwner: .player2,
                                            andAnimal: .rat))
        
        let dog2 : Cell = Cell(ofType: .jungle,
                               piece: Piece(withOwner: .player2,
                                            andAnimal: .dog))
        
        let cat2 : Cell = Cell(ofType: .jungle,
                               piece: Piece(withOwner: .player2,
                                            andAnimal: .cat))
        
        let elephant2 : Cell = Cell(ofType: .jungle,
                                    piece: Piece(withOwner: .player2,
                                                 andAnimal: .elephant))
        
        let wolf2 : Cell = Cell(ofType: .jungle,
                                piece: Piece(withOwner: .player2,
                                             andAnimal: .wolf))
        
        let leopard2 : Cell = Cell(ofType: .jungle,
                                   piece: Piece(withOwner: .player2,
                                                andAnimal: .leopard))
        
        let tiger2 : Cell = Cell(ofType: .jungle,
                                 piece: Piece(withOwner: .player2,
                                              andAnimal: .tiger))
        
        
        let jungle : Cell = Cell(ofType: .jungle,
                                 piece: Piece(withOwner: .noOne,
                                              andAnimal: .dog))
        
        let water : Cell = Cell(ofType: .water,
                                piece: Piece(withOwner: .noOne,
                                             andAnimal: .dog))
        
        let den : Cell = Cell(ofType: .den,
                              piece: Piece(withOwner: .noOne,
                                           andAnimal: .dog))
        
        let trap : Cell = Cell(ofType: .trap,
                               piece: Piece(withOwner: .noOne,
                                            andAnimal: .dog))
        
        let finalList : [[Cell]] = [
            [lion1,jungle,trap,den,trap,jungle,tiger1],
            [jungle,dog1,jungle,trap,jungle,cat1,jungle],
            [rat1,jungle,leopard1,jungle,wolf1,jungle,elephant1],
            [jungle,water,water,jungle,water,water,jungle],
            [jungle,water,water,jungle,water,water,jungle],
            [jungle,water,water,jungle,water,water,jungle],
            [jungle,water,water,jungle,water,water,jungle],
            [elephant2,jungle,wolf2,jungle,leopard2,jungle,rat2],
            [jungle, cat2,jungle,trap,jungle,dog2,jungle],
            [tiger2 ,jungle,trap,den,trap,jungle,lion2],
        ]
        
        board = Board(withGrid: finalList)
    }
    
    func testInitBoardOk() {
        let jungle : Cell = Cell(ofType: .jungle,
                                 piece: Piece(withOwner: .noOne,
                                              andAnimal: .dog))
        let finalList : [[Cell]] = [
            [jungle,jungle,jungle],
            [jungle,jungle,jungle],
        ]
        let boardTest = Board(withGrid: finalList)    
        XCTAssertNotNil(boardTest)
    }
    
    func testInitBoardNotOk() {
        let jungle : Cell = Cell(ofType: .jungle,
                                 piece: Piece(withOwner: .noOne,
                                              andAnimal: .dog))
        let finalList : [[Cell]] = [
            [jungle,jungle],
            [jungle,jungle,jungle],
        ]
        let boardTest = Board(withGrid: finalList)
        XCTAssertNil(boardTest)
    }
    
    func testRemovePieceAnormal() {
        func expect(row : Int,column : Int, resultat : BoardResult){
            XCTAssertEqual(board.remove(atRow: row, andColumn: column), resultat)

        }
        expect(row: 100, column: 0, resultat: .faild(reason: .outOfBounds))
        expect(row: -100, column: 0, resultat: .faild(reason: .outOfBounds))
    }
    
    func testRemovePieceNormal() {
        func expect(row : Int,column : Int, resultat : BoardResult){
            XCTAssertEqual(board.remove(atRow: row, andColumn: column), resultat)
            XCTAssertNil(board.grid[row][column].piece)
        }
        expect(row: 0, column: 0, resultat: .ok)
    }
    
    func testInsertPieceOkNill() {
        XCTAssertEqual(board.insert(piece: nil, atRow: 3, andColumn: 0), .ok)
    }
    
    func testInsertPieceOk() {
        XCTAssertEqual(board.insert(piece: Piece(withOwner: .player1, andAnimal: .cat), atRow: 4, andColumn: 0), .ok)
        XCTAssertEqual(board.grid[4][0].piece,Piece(withOwner: .player1, andAnimal: .cat))
    }
    
    func testInsertPieceCellNotEmpty() {
        XCTAssertEqual(board.insert(piece: nil, atRow: 0, andColumn: 0), .faild(reason: .cellNotEmpty))
    }
    
    func testInsertPieceCellOutOfBoundsPositive() {
        XCTAssertEqual(board.insert(piece: nil, atRow: 100, andColumn: 0), .faild(reason: .outOfBounds))
    }
    
    func testInsertPieceCellOutOfBoundsNegatif() {
        XCTAssertEqual(board.insert(piece: nil, atRow: -100, andColumn: 0), .faild(reason: .outOfBounds))
    }
    
    func testCountPiecesOfPlayer() {
        XCTAssertEqual(board.countPieces(of: .player1), 8)
        XCTAssertEqual(board.countPieces(of: .player2), 8)
    }
    
    func testCountPieces() {
        XCTAssertEqual(board.countPieces().player1, 8)
        XCTAssertEqual(board.countPieces().player2, 8)
    }
    
    func testPerformanceBoardInit() throws {
        self.measure {
            let lion1 : Cell = Cell(ofType: .jungle,
                                    piece: Piece(withOwner: .player1,
                                                 andAnimal: .lion))
            
            let rat1 : Cell = Cell(ofType: .jungle,
                                   piece: Piece(withOwner: .player1,
                                                andAnimal: .rat))
            
            let dog1 : Cell = Cell(ofType: .jungle,
                                   piece: Piece(withOwner: .player1,
                                                andAnimal: .dog))
            
            let cat1 : Cell = Cell(ofType: .jungle,
                                   piece: Piece(withOwner: .player1,
                                                andAnimal: .cat))
            
            let elephant1 : Cell = Cell(ofType: .jungle,
                                        piece: Piece(withOwner: .player1,
                                                     andAnimal: .elephant))
            
            let wolf1 : Cell = Cell(ofType: .jungle,
                                    piece: Piece(withOwner: .player1,
                                                 andAnimal: .wolf))
            
            let leopard1 : Cell = Cell(ofType: .jungle,
                                       piece: Piece(withOwner: .player1,
                                                    andAnimal: .leopard))
            
            let tiger1 : Cell = Cell(ofType: .jungle,
                                     piece: Piece(withOwner: .player1,
                                                  andAnimal: .tiger))
            
            let lion2 : Cell = Cell(ofType: .jungle,
                                    piece: Piece(withOwner: .player2,
                                                 andAnimal: .lion))
            
            let rat2 : Cell = Cell(ofType: .jungle,
                                   piece: Piece(withOwner: .player2,
                                                andAnimal: .rat))
            
            let dog2 : Cell = Cell(ofType: .jungle,
                                   piece: Piece(withOwner: .player2,
                                                andAnimal: .dog))
            
            let cat2 : Cell = Cell(ofType: .jungle,
                                   piece: Piece(withOwner: .player2,
                                                andAnimal: .cat))
            
            let elephant2 : Cell = Cell(ofType: .jungle,
                                        piece: Piece(withOwner: .player2,
                                                     andAnimal: .elephant))
            
            let wolf2 : Cell = Cell(ofType: .jungle,
                                    piece: Piece(withOwner: .player2,
                                                 andAnimal: .wolf))
            
            let leopard2 : Cell = Cell(ofType: .jungle,
                                       piece: Piece(withOwner: .player2,
                                                    andAnimal: .leopard))
            
            let tiger2 : Cell = Cell(ofType: .jungle,
                                     piece: Piece(withOwner: .player2,
                                                  andAnimal: .tiger))
            
            
            let jungle : Cell = Cell(ofType: .jungle,
                                     piece: Piece(withOwner: .noOne,
                                                  andAnimal: .dog))
            
            let water : Cell = Cell(ofType: .water,
                                    piece: Piece(withOwner: .noOne,
                                                 andAnimal: .dog))
            
            let den : Cell = Cell(ofType: .den,
                                  piece: Piece(withOwner: .noOne,
                                               andAnimal: .dog))
            
            let trap : Cell = Cell(ofType: .trap,
                                   piece: Piece(withOwner: .noOne,
                                                andAnimal: .dog))
            
            let finalList : [[Cell]] = [
                [lion1,jungle,trap,den,trap,jungle,tiger1],
                [jungle,dog1,jungle,trap,jungle,cat1,jungle],
                [rat1,jungle,leopard1,jungle,wolf1,jungle,elephant1],
                [jungle,water,water,jungle,water,water,jungle],
                [jungle,water,water,jungle,water,water,jungle],
                [jungle,water,water,jungle,water,water,jungle],
                [jungle,water,water,jungle,water,water,jungle],
                [elephant2,jungle,wolf2,jungle,leopard2,jungle,rat2],
                [jungle, cat2,jungle,trap,jungle,dog2,jungle],
                [tiger2 ,jungle,trap,den,trap,jungle,lion2],
            ]
            
            _ = Board(withGrid: finalList)
        }
    }
    
    func testRemoveAndInsert() throws {
        self.measure {
            _ = board.remove(atRow: 0, andColumn: 0)
            _ = board.insert(piece: Piece(withOwner: .player2, andAnimal: .rat), atRow: 0, andColumn: 0)
        }
    }
}
