import Model
import XCTest

final class PlayerTests: XCTestCase {

    override func setUp() {
         
    }
    
    func testPlayer() throws {
        XCTAssertNil(Player(withName: "test", andId: .noOne))
    }
    
    func testRandomPlayer() throws {
        let randomPlayer = RandomPlayer(withName: "test", andId: .player1)
        let board = VerySimpleRules.createBoard()
        let rules = VerySimpleRules()
        XCTAssertNotNil(randomPlayer?.chooseMove(in: board, with: rules))
    }
    
    func testIAPlayer() throws {
        let randomPlayer = IAPlayer(withName: "test", andId: .player1)
        let board = VerySimpleRules.createBoard()
        let rules = VerySimpleRules()
        XCTAssertNotNil(randomPlayer?.chooseMove(in: board, with: rules))
    }
    
    func testHumanPlayerNormal() throws {
        let move = Move(owner: .player1, rowOrigin: 0, columnOrigin: 1, rowDestination: 1, columnDestination: 1)

        func input(humanPlayer : HumanPlayer) -> Move?{
            return move
        }
        
        let humanPlayer = HumanPlayer(withName: "test", andId: .player1, andInputMethod: input)

        let board = VerySimpleRules.createBoard()
        let rules = VerySimpleRules()
        XCTAssertEqual(humanPlayer?.chooseMove(in: board, with: rules),move)
    }
    
    func testHumanPlayerAnormal() throws {
        let move = Move(owner: .player1, rowOrigin: 1, columnOrigin: 1, rowDestination: 1, columnDestination: 1)

        func input(humanPlayer : HumanPlayer) -> Move?{
            return move
        }
        
        let humanPlayer = HumanPlayer(withName: "test", andId: .player1, andInputMethod: input)

        let board = VerySimpleRules.createBoard()
        let rules = VerySimpleRules()
        XCTAssertEqual(humanPlayer?.chooseMove(in: board, with: rules),nil)
    }
    
    func testHumanPlayerAnormalNil() throws {

        func input(humanPlayer : HumanPlayer) -> Move?{
            return nil
        }
        
        let humanPlayer = HumanPlayer(withName: "test", andId: .player1, andInputMethod: input)

        let board = VerySimpleRules.createBoard()
        let rules = VerySimpleRules()
        XCTAssertEqual(humanPlayer?.chooseMove(in: board, with: rules),nil)
    }
    
}
