import XCTest
@testable import Model

final class RulesTests: XCTestCase {
    var lion1: Cell!
    var rat1: Cell!
    var cat1: Cell!
    var elephant1: Cell!
    var tiger1: Cell!

    var lion2: Cell!
    var rat2: Cell!
    var cat2: Cell!
    var elephant2: Cell!
    var tiger2: Cell!

    var jungle: Cell!
    var den1: Cell!
    var den2: Cell!

    
    var myCells : [[Cell]]!
    var myBoard : Board!
    
    override func setUp() {
         lion1 = Cell(ofType: .jungle,
                piece: Piece(withOwner: .player1,
                andAnimal: .lion))

         rat1 = Cell(ofType: .jungle,
                piece: Piece(withOwner: .player1,
                andAnimal: .rat))

         cat1 = Cell(ofType: .jungle,
                piece: Piece(withOwner: .player1,
                andAnimal: .cat))

         elephant1 = Cell(ofType: .jungle,
                piece: Piece(withOwner: .player1,
                andAnimal: .elephant))

         tiger1 = Cell(ofType: .jungle,
                piece: Piece(withOwner: .player1,
                andAnimal: .tiger))

         lion2 = Cell(ofType: .jungle,
                piece: Piece(withOwner: .player2,
                andAnimal: .lion))

         rat2 = Cell(ofType: .jungle,
                piece: Piece(withOwner: .player2,
                andAnimal: .rat))

         cat2 = Cell(ofType: .jungle,
                piece: Piece(withOwner: .player2,
                andAnimal: .cat))

         elephant2 = Cell(ofType: .jungle,
                piece: Piece(withOwner: .player2,
                andAnimal: .elephant))

         tiger2 = Cell(ofType: .jungle,
                piece: Piece(withOwner: .player2,
                andAnimal: .tiger))

         jungle = Cell(ofType: .jungle)

        den1 = Cell(ofType: .den,
                    initialOwner: .player1)
        den2 = Cell(ofType: .den,
                initialOwner: .player2)
        myCells = [
            [jungle,lion1,den1,tiger1,jungle],
            [rat1,jungle,cat1,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [elephant2, jungle, cat2, jungle, rat2],
            [jungle, tiger2, den2, lion2, jungle],
        ]
        
        myBoard = Board(withGrid: myCells)
    }
    
    func testCreateBoard(){
        XCTAssertEqual(myBoard,VerySimpleRules.createBoard())
    }
    
    func testGetMovesZero(){
        myCells = [
            [jungle,lion1,den1,tiger1,jungle],
            [rat1,jungle,cat1,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [jungle, jungle, jungle, jungle, jungle],
            [jungle, jungle, den2, jungle, jungle],
        ]
        myBoard = Board(withGrid: myCells)
        let rules = VerySimpleRules(occurence: [:], historic: [])
        XCTAssertEqual(rules.getMoves(fromBoard: myBoard!, ofTheOwner: .player2).count,0)
    }
    
    func testGetMovesFour(){
        myCells = [
            [jungle,lion1,den1,tiger1,jungle],
            [rat1,jungle,cat1,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [jungle, jungle, cat2, jungle, jungle],
            [jungle, jungle, den2, jungle, jungle],
        ]
        myBoard = Board(withGrid: myCells)
        let rules = VerySimpleRules(occurence: [:], historic: [])
        XCTAssertEqual(rules.getMoves(fromBoard: myBoard!, ofTheOwner: .player2).count,3)
        
        myCells = [
            [jungle,lion1,den1,tiger1,jungle],
            [rat1,jungle,cat1,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [jungle, jungle, jungle, jungle, jungle],
            [cat2, jungle, den2, jungle, jungle],
        ]
        myBoard = Board(withGrid: myCells)
        XCTAssertEqual(rules.getMoves(fromBoard: myBoard!, ofTheOwner: .player2).count,2)
        
        myCells = [
            [jungle,lion1,den1,tiger1,jungle],
            [jungle,jungle,cat1,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [rat1, jungle, jungle, jungle, jungle],
            [elephant2, jungle, den2, jungle, jungle],
        ]
        myBoard = Board(withGrid: myCells)
        XCTAssertEqual(rules.getMoves(fromBoard: myBoard!, ofTheOwner: .player2).count,1)
        
        myCells = [
            [jungle,lion1,den1,tiger1,jungle],
            [jungle,jungle,cat1,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [elephant1, jungle, jungle, jungle, jungle],
            [rat2, jungle, den2, jungle, jungle],
        ]
        myBoard = Board(withGrid: myCells)
        XCTAssertEqual(rules.getMoves(fromBoard: myBoard!, ofTheOwner: .player2).count,2)
        
        myCells = [
            [jungle,lion1,den1,tiger1,jungle],
            [jungle,jungle,jungle,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [rat1, jungle, jungle, jungle, jungle],
            [tiger2, lion1, den2, jungle, jungle],
        ]
        myBoard = Board(withGrid: myCells)
        XCTAssertEqual(rules.getMoves(fromBoard: myBoard!, ofTheOwner: .player2).count,1)
        
        myCells = [
            [jungle,lion1,den1,tiger1,jungle],
            [jungle,jungle,jungle,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [jungle, jungle, jungle, jungle, jungle],
            [jungle, jungle, den2, jungle, jungle],
        ]
        myBoard = Board(withGrid: myCells)
        XCTAssertEqual(rules.getMoves(fromBoard: myBoard!, ofTheOwner: .player2).count,0)
    }
    
    func testIsValidMove(){
        myCells = [
            [jungle,lion1,den1,tiger1,jungle],
            [rat1,jungle,cat1,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [jungle, jungle, cat2, jungle, jungle],
            [jungle, jungle, den2, jungle, jungle],
        ]
        myBoard = Board(withGrid: myCells)
        let rules = VerySimpleRules(occurence: [:], historic: [])
        var  move = Move(owner: .player2, rowOrigin: 3, columnOrigin: 2, rowDestination: 2, columnDestination: 2)
        XCTAssertEqual(rules.isMoveValid(fromBoard: myBoard, move), true)
        
        myCells = [
            [jungle,lion1,den1,tiger1,jungle],
            [jungle,jungle,cat1,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [rat1, jungle, jungle, jungle, jungle],
            [elephant2, jungle, den2, jungle, jungle],
        ]
        myBoard = Board(withGrid: myCells)
        move = Move(owner: .player2, rowOrigin: 4, columnOrigin: 0, rowDestination: 3, columnDestination: 0)
        XCTAssertEqual(rules.isMoveValid(fromBoard: myBoard, move), false)
        
        myCells = [
            [jungle,lion1,den1,tiger1,jungle],
            [jungle,jungle,cat1,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [elephant1, jungle, jungle, jungle, jungle],
            [rat2, jungle, den2, jungle, jungle],
        ]
        myBoard = Board(withGrid: myCells)
        
        move = Move(owner: .player2, rowOrigin: 4, columnOrigin: 0, rowDestination: 3, columnDestination: 0)
        XCTAssertEqual(rules.isMoveValid(fromBoard: myBoard, move), true)
        
        myCells = [
            [jungle,lion1,den1,tiger1,jungle],
            [jungle,jungle,jungle,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [rat1, jungle, jungle, jungle, jungle],
            [tiger2, lion1, den2, jungle, jungle],
        ]
        myBoard = Board(withGrid: myCells) 
        move = Move(owner: .player2, rowOrigin: 4, columnOrigin: 0, rowDestination: 3, columnDestination: 0)
        XCTAssertEqual(rules.isMoveValid(fromBoard: myBoard, move), true)
        
        myCells = [
            [jungle,lion1,den1,tiger1,jungle],
            [jungle,jungle,jungle,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [rat1, jungle, jungle, jungle, jungle],
            [tiger2, lion1, den2, jungle, jungle],
        ]
        myBoard = Board(withGrid: myCells)
        move = Move(owner: .player2, rowOrigin: 4, columnOrigin: 0, rowDestination: 4, columnDestination: 1)
        XCTAssertEqual(rules.isMoveValid(fromBoard: myBoard, move), false)
        
        myCells = [
            [jungle,lion1,den1,tiger1,jungle],
            [jungle,jungle,jungle,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [jungle, jungle, jungle, jungle, jungle],
            [jungle, jungle, den2, jungle, jungle],
        ]
        myBoard = Board(withGrid: myCells)
        XCTAssertEqual(rules.getMoves(fromBoard: myBoard!, ofTheOwner: .player2).count,0)
        
        move = Move(owner: .player2, rowOrigin: 4, columnOrigin: 0, rowDestination: 4, columnDestination: 1)
        XCTAssertEqual(rules.isMoveValid(fromBoard: myBoard, move), false)
        
        move = Move(owner: .player1, rowOrigin: 0, columnOrigin: 1, rowDestination: 0, columnDestination: 2)
        XCTAssertEqual(rules.isMoveValid(fromBoard: myBoard, move), false)
        
        move = Move(owner: .player2, rowOrigin: -4, columnOrigin: 0, rowDestination: 4, columnDestination: 1)
        XCTAssertEqual(rules.isMoveValid(fromBoard: myBoard, move), false)
        
        move = Move(owner: .player2, rowOrigin: 400, columnOrigin: 0, rowDestination: 4, columnDestination: 1)
        XCTAssertEqual(rules.isMoveValid(fromBoard: myBoard, move), false)
    }
    
    func testGetNextPlayer(){
        var rules = VerySimpleRules(occurence: [:], historic: [])
        XCTAssertEqual(rules.getNextPlayer(), .player1)
        
        var  move = Move(owner: .player1, rowOrigin: 3, columnOrigin: 2, rowDestination: 2, columnDestination: 2)
        rules = VerySimpleRules(occurence: [:], historic: [move])
        XCTAssertEqual(rules.getNextPlayer(), .player2)
        
        move = Move(owner: .player2, rowOrigin: 3, columnOrigin: 2, rowDestination: 2, columnDestination: 2)
        rules = VerySimpleRules(occurence: [:], historic: [move])
        XCTAssertEqual(rules.getNextPlayer(), .player1)
    }
    
    func testMovePlayed(){
        var rules = VerySimpleRules(occurence: [:], historic: [])
        let  move = Move(owner: .player1, rowOrigin: 3, columnOrigin: 2, rowDestination: 2, columnDestination: 2)
        XCTAssertEqual(rules.historic.count, 0)
        rules.playedMove(move, currentBoard: myBoard, newBoard: myBoard)
        XCTAssertEqual(rules.historic.count, 1)
    }
    
    
    func testIsGameOver(){
        
        let rules = VerySimpleRules(occurence: [:], historic: [])
        var result = rules.isGameOver(fromBoard: myBoard, atRow: 1, andColumn: 0)
        XCTAssertEqual(result.isWinner, false)
        XCTAssertEqual(result.theResult, .notFinished)
        let denWinner = Cell(ofType: .den,
                             initialOwner: .player2, piece: lion1.piece)
        myCells = [
            [jungle,lion1,den1,tiger1,jungle],
            [rat1,jungle,cat1,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [jungle, jungle, cat2, jungle, jungle],
            [jungle, jungle, denWinner, jungle, jungle],
        ]
        myBoard = Board(withGrid: myCells)
        result = rules.isGameOver(fromBoard: myBoard, atRow: 4, andColumn: 2)
        XCTAssertEqual(result.isWinner, true)
        XCTAssertEqual(result.theResult, .winner(.player1, .denReached))
        
        myCells = [
            [jungle,lion1,den1,tiger1,jungle],
            [rat1,jungle,cat1,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [jungle, jungle, jungle, jungle, jungle],
            [jungle, jungle, den2, jungle, jungle],
        ]
        myBoard = Board(withGrid: myCells)
        
        result = rules.isGameOver(fromBoard: myBoard, atRow: 1, andColumn: 0)
        XCTAssertEqual(result.isWinner, true)
        XCTAssertEqual(result.theResult, .winner(.player1, .noMorePieces))
        
        myCells = [
            [jungle,jungle,den1,jungle,jungle],
            [rat1,jungle,cat1,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [lion1, jungle, jungle, jungle, jungle],
            [rat2, tiger1, den2, jungle, jungle],
        ]
        myBoard = Board(withGrid: myCells)
        
        result = rules.isGameOver(fromBoard: myBoard, atRow: 1, andColumn: 0)
        XCTAssertEqual(result.isWinner, true)
        XCTAssertEqual(result.theResult, .winner(.player1, .noMovesLeft))
    }
    
    func testCheckBoard(){
        var denTest = Cell(ofType: .den,
                    initialOwner: .player1,
                    piece: Piece(withOwner: .player1, andAnimal: .rat))
        myCells = [
            [jungle,lion1,denTest,tiger1,jungle],
            [jungle,jungle,cat1,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [elephant2, jungle, cat2, jungle, rat2],
            [jungle, tiger2, den2, lion2, jungle],
        ]
        myBoard = Board(withGrid: myCells)
        
        XCTAssertThrowsError(try VerySimpleRules.checkBoard(myBoard)) { (error) in
            XCTAssertEqual(error as? InvalidBoardError, InvalidBoardError.pieceNotAllowedOnThisCell(denTest.piece!, denTest))
        }
        
        denTest = Cell(ofType: .den,
                    initialOwner: .player2,
                    piece: Piece(withOwner: .player2, andAnimal: .rat))
        myCells = [
            [jungle,lion1,den1,tiger1,jungle],
            [jungle,jungle,cat1,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [elephant2, jungle, cat2, jungle, rat2],
            [jungle, tiger2, denTest, lion2, jungle],
        ]
        myBoard = Board(withGrid: myCells)
        
        XCTAssertThrowsError(try VerySimpleRules.checkBoard(myBoard)) { (error) in
            XCTAssertEqual(error as? InvalidBoardError, InvalidBoardError.pieceNotAllowedOnThisCell(denTest.piece!, denTest))
        }
        
        myCells = [
            [jungle,lion1,jungle,tiger1,jungle],
            [rat1,jungle,cat1,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [elephant2, jungle, cat2, jungle, rat2],
            [jungle, tiger2, den2, lion2, jungle],
        ]
        myBoard = Board(withGrid: myCells)
        
        XCTAssertThrowsError(try VerySimpleRules.checkBoard(myBoard)) { (error) in
            XCTAssertEqual(error as? InvalidBoardError, InvalidBoardError.badCellType(.jungle, 0, 2))
        }
        
        myCells = [
            [jungle,lion1,den1,tiger1,jungle],
            [rat1,jungle,cat1,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [jungle, jungle, cat2, jungle, jungle],
            [jungle, jungle, jungle, jungle, jungle],
        ]
        myBoard = Board(withGrid: myCells)
        
        XCTAssertThrowsError(try VerySimpleRules.checkBoard(myBoard)) { (error) in
            XCTAssertEqual(error as? InvalidBoardError, InvalidBoardError.badCellType(.jungle, 4, 2))
        }
        let water = Cell(ofType: .water)
        myCells = [
            [water,lion1,den1,tiger1,jungle],
            [rat1,jungle,cat1,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [jungle, jungle, cat2, jungle, jungle],
            [jungle, jungle, jungle, jungle, jungle],
        ]
        myBoard = Board(withGrid: myCells)
        
        XCTAssertThrowsError(try VerySimpleRules.checkBoard(myBoard)) { (error) in
            XCTAssertEqual(error as? InvalidBoardError, InvalidBoardError.badCellType(.water, 0, 0))
        }
        
        myCells = [
            [jungle,lion1,den1,tiger1,jungle],
            [jungle,lion1,den1,tiger1,jungle],
            [rat1,jungle,cat1,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [jungle, jungle, cat2, jungle, jungle],
            [jungle, jungle, jungle, jungle, jungle],
        ]
        myBoard = Board(withGrid: myCells)
        
        XCTAssertThrowsError(try VerySimpleRules.checkBoard(myBoard)) { (error) in
            XCTAssertEqual(error as? InvalidBoardError, InvalidBoardError.badDimensions(5, 5))
        }
        
        var piece  = Piece(withOwner: .noOne, andAnimal: .cat)
        cat1.piece = piece
        myCells = [
            [jungle,lion1,den1,tiger1,jungle],
            [rat1,jungle,cat1,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [jungle, jungle, cat2, jungle, jungle],
            [jungle, jungle, jungle, jungle, jungle],
        ]
        myBoard = Board(withGrid: myCells)
        
        XCTAssertThrowsError(try VerySimpleRules.checkBoard(myBoard)) { (error) in
            XCTAssertEqual(error as? InvalidBoardError, InvalidBoardError.pieceWithNoOwner(cat1.piece!))
        }
        
        piece  = Piece(withOwner: .player1, andAnimal: .cat)
        cat1.piece = piece
        lion1.piece  =  piece
        myCells = [
            [jungle,lion1,den1,tiger1,jungle],
            [rat1,jungle,cat1,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [jungle, jungle, cat2, jungle, jungle],
            [jungle, jungle, jungle, jungle, jungle],
        ]
        myBoard = Board(withGrid: myCells)
        
        XCTAssertThrowsError(try VerySimpleRules.checkBoard(myBoard)) { (error) in
            XCTAssertEqual(error as? InvalidBoardError, InvalidBoardError.multipleOccurrencesOfSamePiece(cat1.piece!))
        }

        
    }

}
