import Foundation

public protocol Rules {
    var occurence : [Board: Int] {get}
    var historic : [Move] {get}
    static func createBoard() -> Board
    static func checkBoard(_ b: Board) throws
    func getNextPlayer() -> Owner
    func getMoves(fromBoard board: Board,ofTheOwner owner: Owner) -> [Move]
    func getMoves(fromBoard board: Board,ofTheOwner owner: Owner,atRow row: Int, andAtColumn: Int) -> [Move]
    func isMoveValid(fromBoard board: Board, atRowOrigin rowOrigin: Int, atColumnOrigin columnOrigin: Int, atRowDestination rowDestination: Int, andColumnDestination columnDestination: Int) -> Bool
    func isMoveValid(fromBoard board: Board, _ move: Move) -> Bool
    func isGameOver(fromBoard board: Board, atRow row: Int, andColumn column: Int) -> (isWinner : Bool , theResult : Result)
    mutating func playedMove(_ move: Move, currentBoard: Board, newBoard: Board)
}
