import Foundation

///This struct represent the board of ShouDouQi
public struct Board : Hashable{
    public let nbRows : Int
    public let nbColums : Int
    public private(set) var grid: [[Cell]]
    
    ///works only if all lines have the same size
    public init?(withGrid grid: [[Cell]]) {
        guard grid.hasSameRowAndColumnSize() else { return nil }
        self.nbRows = grid.count
        self.nbColums = grid.first!.count
        self.grid = grid
    }
    
    ///count the peices of a player
    public func countPieces(of player : Owner) -> Int {grid.countPieces(of: player)}
    
    ///count the peices of both player
    public func countPieces() -> (player1 : Int,player2: Int) { ( grid.countPieces(of: .player1), grid.countPieces(of: .player2) ) }
    
    ///insert a piece in a place on the board
    public mutating func insert(piece : Piece?, atRow row : Int, andColumn column : Int) -> BoardResult {
        guard  column < nbColums && row < nbRows && row >= 0 && column >= 0 else {return BoardResult.faild(reason: .outOfBounds)}
                
        guard grid[row][column].piece == nil ||  grid[row][column].piece!.isEmpty else {return BoardResult.faild(reason: .cellNotEmpty) }
        
        grid[row][column].piece = piece
        
        return .ok
        
    }
    
    ///remove a piece from a place on the board
    public mutating func remove(atRow row : Int, andColumn column : Int) -> BoardResult {
        guard  column < nbColums && row < nbRows && row >= 0 && column >= 0 else {return BoardResult.faild(reason: .outOfBounds)}
        
        guard grid[row][column].piece != nil else {return BoardResult.faild(reason: .cellEmpty) }
        
        guard !(grid[row][column].piece!.isEmpty) else {return BoardResult.faild(reason: .cellEmpty) }
        
        grid[row][column].piece = nil
        
        return .ok
        
    }
}

extension Array where Element == Array<Cell> {
    
    ///all lines have the same size
    func hasSameRowAndColumnSize() -> Bool {
        guard let firstRowSize = self.first?.count else { return false }
        return self.allSatisfy({ $0.count == firstRowSize })
    }
    
    ///counts how many pieces there are
    func countPieces(of player : Owner) -> Int {
        return self.flatMap{$0.filter{ $0.piece?.owner == player}}.count
    }
    
}

extension Piece{
    var isEmpty : Bool { self.owner == .noOne}
}
