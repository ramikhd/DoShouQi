import Foundation

///This struct represent the Game  events
public enum GameEvents{
    case nextPlayerTurn((Player, Board) -> Void)
    case gameStarted((Board) -> Void)
    case invalidMoveEntered(() -> Void)
    case gameResultPublished((Result, Board) -> Void)
    case moveHasBeenChoosen((Player, Move) -> Void)
    case boardChanged((Board) -> Void)
    case saveGame((Game) -> Void)

}

///This struct represent the Game  manger
public struct Game{
    public var rule : Rules
    public let player1 : Player
    public let player2 : Player
    public var board : Board
    
    ///Choose the way you want to subsxcribe Methode swift spirit <3
    private var gameEvents : [GameEvents] = []
    ///Choose the way you want to subsxcribe Methode classic
    private var gameStarted: [((Board) -> Void)] = []
    private var nextPlayerTurn: [((Player, Board) -> Void)] = []
    private var invalidMoveEntered: [(() -> Void)] = []
    private var gameResultPublished: [((Result, Board) -> Void)] = []
    private var moveHasBeenChoosen: [((Player, Move) -> Void)] = []
    private var boardChanged: [((Board) -> Void)] = []
    
    public mutating func addListener(_ listenet: GameEvents) {
        gameEvents.append(listenet)
    }
    
    public init(withRules Rules : Rules,andPlayer1 Player1: Player, andPlayer2 Player2: Player) throws {
        guard !Player1.id == Player2.id  else {
            throw GameError.badPlayerId(name: "")
        }
        rule = Rules
        player1 = Player1
        player2 = Player2
    
        board = type(of: rule).createBoard()
    }
    
    public mutating func start() throws{
        gameEvents.forEach{if case .gameStarted(let handler) = $0{handler(board)}}
        gameStarted.forEach{ $0(board) }
        try gameLoop()
    }
    
    mutating func makeMove(player: Player) -> (isWinner: Bool, theResult: Result) {
        var move = player.chooseMove(in: board, with: rule)
        
        while move == nil {
            gameEvents.forEach{if case .saveGame(let handler) = $0{handler(self)}}
            invalidMoveEntered.forEach{ $0() }
            gameEvents.forEach{if case .invalidMoveEntered(let handler) = $0{handler()}}

            move = player.chooseMove(in: board, with: rule)
        }
        
        gameEvents.forEach{if case .saveGame(let handler) = $0{handler(self)}}
        moveHasBeenChoosen.forEach{ $0(player,move!) }
        gameEvents.forEach{if case .moveHasBeenChoosen(let handler) = $0{handler(player,move!)}}

        let oldBoard = board
        let _ = board.remove(atRow: move!.rowDestination, andColumn: move!.columnDestination)
        let _ = board.insert(piece: board.grid[move!.rowOrigin][move!.columnOrigin].piece, atRow: move!.rowDestination, andColumn: move!.columnDestination)
        let _ = board.remove(atRow: move!.rowOrigin, andColumn: move!.columnOrigin)
        rule.playedMove(move!, currentBoard: oldBoard, newBoard: board)
        
        gameEvents.forEach{if case .saveGame(let handler) = $0{handler(self)}}
        boardChanged.forEach{ $0(board) }
        gameEvents.forEach{if case .boardChanged(let handler) = $0{handler(board)}}



        let gameOverResult = rule.isGameOver(fromBoard: board,
                                              atRow: move!.rowDestination,
                                              andColumn: move!.columnDestination)
        
        return gameOverResult
    }

    mutating func gameLoop() throws {
        var gameResult : (isWinner: Bool, theResult: Result) = rule.historic.count > 0 ?
        rule.isGameOver(fromBoard: board,
                        atRow: rule.historic.last!.rowDestination,
                        andColumn: rule.historic.last!.columnDestination) :(false,.notFinished)
        
        var player : Player = player1
        var lastPlayer : Owner = .noOne
        
        while !gameResult.isWinner {
            if lastPlayer == player.id {
                throw GameError.nextPlayerError
            }
            
            gameEvents.forEach{if case .saveGame(let handler) = $0{handler(self)}}
            gameEvents.forEach{if case .nextPlayerTurn(let handler) = $0{handler(player, board)}}
            nextPlayerTurn.forEach{ $0(player,board) }

            gameResult = makeMove(player: player)
            
            gameEvents.forEach{if case .saveGame(let handler) = $0{handler(self)}}
            gameEvents.forEach{if case .gameResultPublished(let handler) = $0{handler(gameResult.theResult,board)}}
            gameResultPublished.forEach{ $0(gameResult.theResult,board) }
            
            lastPlayer = player.id
            player = player.id == .player1 ? player2 : player1
        }
    }
}
