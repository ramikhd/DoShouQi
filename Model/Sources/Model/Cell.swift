import Foundation

///This struct represent a cell of the board in ShouDouQi
///So it is represented by the cell type and the piece on it also the intial owner it is for a the trap
public struct Cell : CustomStringConvertible,Hashable{
    public let cellType : CellType
    public let initialOwner : Owner
    public var piece : Piece?
    
    public init(ofType cellType: CellType, initialOwner: Owner = .noOne, piece: Piece? = nil) {
        self.cellType = cellType
        self.initialOwner = initialOwner
        self.piece = piece
    }
    public var description: String { "(\(piece?.description ?? "ø")) on \(cellType), \(initialOwner)" }
}
