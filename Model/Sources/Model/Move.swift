import Foundation

///This struct represent a move by the origin cell, the destination and the owner
public struct Move : Equatable {
    public var owner: Owner
    public var rowOrigin: Int
    public var columnOrigin: Int
    public var rowDestination: Int
    public var columnDestination: Int
   
    public init(owner: Owner, rowOrigin: Int, columnOrigin: Int, rowDestination: Int, columnDestination: Int) {
        self.owner = owner
        self.rowOrigin = rowOrigin
        self.columnOrigin = columnOrigin
        self.rowDestination = rowDestination
        self.columnDestination = columnDestination
    }
    
    public var description: String{
        return "from (\(rowOrigin),\(columnOrigin)) to (\(rowDestination),\(columnDestination))"
    }
}
