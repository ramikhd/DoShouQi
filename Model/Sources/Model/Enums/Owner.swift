import Foundation

///This enum represent the owner of a cell
public enum Owner : CustomStringConvertible{
    case noOne
    case player1
    case player2
    
    public var description: String{
        switch(self){
        case .noOne:
            return "x"
        case .player1:
            return  "1"
        case .player2:
            return "2"
        }
    }
    
    static prefix func !(owner: Owner) -> Owner {
        switch(owner){
        case .noOne:
            return .noOne
        case .player1:
            return  .player2
        case .player2:
            return .player1
        }
    }
}
