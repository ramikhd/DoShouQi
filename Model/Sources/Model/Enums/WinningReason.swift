import Foundation
///This enum represent the winning reason
public enum WinningReason {
    case unknown
    case denReached
    case noMorePieces
    case tooManyOccurrences
    case noMovesLeft
}
