import Foundation

///This enum represent the reason of a board failling to insert a piece
public enum BoardFailingReason{
    case outOfBounds
    case cellNotEmpty
    case cellEmpty
    case unknown
}
