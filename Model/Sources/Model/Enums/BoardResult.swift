import Foundation

///This enum represent the result of insert or remove a piece
public enum BoardResult : Equatable{
    case unknown
    case ok
    case faild(reason: BoardFailingReason)
}
