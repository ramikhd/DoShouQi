import Foundation

///This enum represent the cell type of the ShouDouQi
public enum CellType{
    case unknown
    case jungle
    case water
    case trap
    case den
}
