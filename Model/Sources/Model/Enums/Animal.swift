import Foundation

///This enum represent the animals of the ShouDouQi game
public enum Animal : Int{
    case rat
    case cat
    case dog
    case wolf
    case leopard
    case tiger
    case lion
    case elephant
}
