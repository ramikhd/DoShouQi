import Foundation

/// Enum representing errors related to invalid boards
public enum InvalidBoardError : Error, Equatable {
    case badDimensions(Int, Int)
    case badCellType(CellType, Int, Int)
    case multipleOccurrencesOfSamePiece(Piece)
    case pieceWithNoOwner(Piece)
    case pieceNotAllowedOnThisCell(Piece, Cell)
}
