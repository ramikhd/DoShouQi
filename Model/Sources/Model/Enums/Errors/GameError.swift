import Foundation

/// Enum representing game errors
public enum GameError : Error {
    case invalidMove
    case nextPlayerError
    case badPlayerId(name : String)

}
