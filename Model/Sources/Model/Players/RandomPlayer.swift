import Foundation

///This class represent a random player in ShouDouQi
public class RandomPlayer : Player {
    public override func chooseMove(in board: Board, with rules: Rules) -> Move? {
       return rules.getMoves(fromBoard: board, ofTheOwner: id).randomElement()
   }
}
