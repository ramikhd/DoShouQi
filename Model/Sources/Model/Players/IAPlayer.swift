import Foundation

///This class represent a IA player in ShouDouQi that choose to move in a den if possible if not it choose to eat an animal if possible if not it do a random move
public class IAPlayer : Player {
    public override func chooseMove(in board: Board, with rules: Rules) -> Move? {
        let moves = rules.getMoves(fromBoard: board, ofTheOwner: id)
        var move : Move?
        
        move = moves.first{board.grid[$0.rowDestination][$0.columnDestination].cellType == .den }
        if move == nil {
            move = moves.first{board.grid[$0.rowDestination][$0.columnDestination].piece != nil }
            if move == nil {
                move = moves.randomElement()
            }
        }
        return move
   }
}
