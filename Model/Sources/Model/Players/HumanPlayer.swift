import Foundation

///This class represent a human player in ShouDouQi which choose how to play in depend of an input methode
public class HumanPlayer : Player{
    public let input: (HumanPlayer) -> Move?

    public init?(withName name: String, andId id: Owner, andInputMethod Input: @escaping (HumanPlayer) -> Move?){
        input = Input
        super.init(withName: name, andId: id)
    }

   public override func chooseMove(in board: Board, with rules: Rules) -> Move? {
       let move = input(self)
       if let move {
           return rules.isMoveValid(fromBoard: board, move) ? move : nil
       }
       return nil
   }
}
