import Foundation

///This struct represent one way to define rules for ShouDouQi
public struct VerySimpleRules : Rules{
    private static let BOARD_SIZE : (row : Int, column : Int) = (5,5)
    private static let DEN_POSITION : (player1 : (row : Int, column : Int),
                                       player2 : (row : Int, column : Int)) = (
                                        (0,2),(4,2)
                                       )

    public var occurence: [Board : Int] = [:]
    
    public var historic: [Move] = []
    
    public init(){
    }
    
    public init(occurence: [Board : Int],historic: [Move]){
        self.occurence = occurence
        self.historic = historic
    }
    
    public static func createBoard() -> Board {
        
        let lion1 : Cell = Cell(ofType: .jungle,
                piece: Piece(withOwner: .player1,
                andAnimal: .lion))

        let rat1 : Cell = Cell(ofType: .jungle,
                piece: Piece(withOwner: .player1,
                andAnimal: .rat))

        let cat1 : Cell = Cell(ofType: .jungle,
                piece: Piece(withOwner: .player1,
                andAnimal: .cat))

        let elephant1 : Cell = Cell(ofType: .jungle,
                piece: Piece(withOwner: .player1,
                andAnimal: .elephant))

        let tiger1 : Cell = Cell(ofType: .jungle,
                piece: Piece(withOwner: .player1,
                andAnimal: .tiger))

        let lion2 : Cell = Cell(ofType: .jungle,
                piece: Piece(withOwner: .player2,
                andAnimal: .lion))

        let rat2 : Cell = Cell(ofType: .jungle,
                piece: Piece(withOwner: .player2,
                andAnimal: .rat))

        let cat2 : Cell = Cell(ofType: .jungle,
                piece: Piece(withOwner: .player2,
                andAnimal: .cat))

        let elephant2 : Cell = Cell(ofType: .jungle,
                piece: Piece(withOwner: .player2,
                andAnimal: .elephant))

        let tiger2 : Cell = Cell(ofType: .jungle,
                piece: Piece(withOwner: .player2,
                andAnimal: .tiger))


        let jungle = Cell(ofType: .jungle)

       let den1 = Cell(ofType: .den,
                   initialOwner: .player1)
       let den2 = Cell(ofType: .den,
               initialOwner: .player2)

        let finalList : [[Cell]] = [
            [jungle,lion1,den1,tiger1,jungle],
            [rat1,jungle,cat1,jungle,elephant1],
            [jungle,jungle,jungle,jungle,jungle],
            [elephant2, jungle, cat2, jungle, rat2],
            [jungle, tiger2, den2, lion2, jungle],
        ]
        return Board(withGrid: finalList)!
    }
    
    public static func checkBoard(_ board: Board) throws {
        
        // Verify that the size of the table is correct
        guard board.grid.first?.count == BOARD_SIZE.column && board.grid.count == BOARD_SIZE.row else {
            throw InvalidBoardError.badDimensions(5,5)
        }

        var encounteredPieces = Set<Piece>()
       
       for i in 0..<board.nbRows {
           for j in 0..<board.nbColums {
               let place = board.grid[i][j]
               if let piece = place.piece {
                   // Verify piece is ownedby some one
                   guard piece.owner != .noOne else {
                       throw InvalidBoardError.pieceWithNoOwner(piece)
                   }

                   // Verify that the piece isnt dupplicated
                   guard encounteredPieces.insert(piece).inserted else {
                       throw InvalidBoardError.multipleOccurrencesOfSamePiece(piece)
                   }
               }

               // Verify that on all the board there is jungle or den
               let trueBoard = VerySimpleRules.createBoard()
               if trueBoard.grid[i][j].cellType != board.grid[i][j].cellType {
                   throw InvalidBoardError.badCellType(board.grid[i][j].cellType, i, j)
               }
               
               
               if board.grid[DEN_POSITION.player1.row][DEN_POSITION.player1.column].piece?.owner == board.grid[DEN_POSITION.player1.row][DEN_POSITION.player1.column].initialOwner{
                   throw InvalidBoardError.pieceNotAllowedOnThisCell(
                    board.grid[DEN_POSITION.player1.row][DEN_POSITION.player1.column].piece!,
                         board.grid[DEN_POSITION.player1.row][DEN_POSITION.player1.column]
                   )
               }
               
               if board.grid[DEN_POSITION.player2.row][DEN_POSITION.player2.column].piece?.owner == board.grid[DEN_POSITION.player2.row][DEN_POSITION.player2.column].initialOwner{
                   throw InvalidBoardError.pieceNotAllowedOnThisCell(
                    board.grid[DEN_POSITION.player2.row][DEN_POSITION.player2.column].piece!,
                         board.grid[DEN_POSITION.player2.row][DEN_POSITION.player2.column]
                   )
               }
               
               
           }
       }
   }
           
    
    public func getNextPlayer() -> Owner {
        return historic.count == 0 ? .player1 : historic.last?.owner == .player1 ? .player2 : .player1
    }
    
    public func getMoves(fromBoard board: Board, ofTheOwner owner: Owner) -> [Move] {
        var moves : [Move] = []
        for i in 0..<board.grid.count{
            for j in 0..<board.grid[i].count{
                let cell = board.grid[i][j]
                if cell.piece?.owner != owner {
                    continue
                }
                moves.append(contentsOf : getMoves(fromBoard: board, ofTheOwner: owner, atRow: i, andAtColumn: j))
            }
        }
        return moves
    }
    
    public func getMoves(fromBoard board: Board, ofTheOwner owner: Owner, atRow row: Int, andAtColumn column: Int) -> [Move] {
        
        var moves : [Move] = []
        let possibleSteps : [(row : Int, column : Int)] = [
            (1,0),
            (-1,0),
            (0,1),
            (0,-1)
        ]
        
        for possibleMove in possibleSteps{
            let move = Move(owner: owner, rowOrigin: row, columnOrigin: column,
                            rowDestination: row + possibleMove.row , columnDestination: column + possibleMove.column)
            if isMoveValid(fromBoard: board, move){
                moves.append(move)
            }
        }
        return moves
    }
    
    public func isMoveValid(fromBoard board: Board, atRowOrigin rowOrigin: Int, atColumnOrigin columnOrigin: Int, atRowDestination rowDestination: Int, andColumnDestination columnDestination: Int) -> Bool {
        return isMoveValid(fromBoard: board,Move(owner: getNextPlayer(), rowOrigin: rowOrigin, columnOrigin: columnOrigin, rowDestination: rowDestination, columnDestination: columnDestination))
    }
    
    public func isMoveValid(fromBoard board: Board, _ move: Move) -> Bool {
        // The cordinates of the move is not out of boundrie
        guard   move.rowDestination < VerySimpleRules.BOARD_SIZE.row &&
                move.columnDestination < VerySimpleRules.BOARD_SIZE.column  &&
                move.rowDestination >= 0 &&
                move.columnDestination >= 0 &&
                move.rowOrigin >= 0 &&
                move.columnOrigin >= 0 &&
                move.rowOrigin < VerySimpleRules.BOARD_SIZE.row &&
                move.columnOrigin < VerySimpleRules.BOARD_SIZE.column else { return false }
        
        // The next piece is owned by the owner and it is a den
        if board.grid[move.rowDestination][move.columnDestination].initialOwner ==
            board.grid[move.rowOrigin][move.columnOrigin].piece?.owner &&
            board.grid[move.rowDestination][move.columnDestination].cellType == .den
        {
            return false
        }
        
        // The next place is not owned by the owner
        guard board.grid[move.rowDestination][move.columnDestination].piece?.owner !=
                board.grid[move.rowOrigin][move.columnOrigin].piece?.owner else {
            return false
        }
        
        // The next piece is not owned by no one
        guard board.grid[move.rowDestination][move.columnDestination].piece?.owner != .noOne else {
            return true
        }
        
        let pieceOriginAnimal = (board.grid[move.rowOrigin][move.columnOrigin].piece?.animal)?.rawValue ?? -1
        let  pieceDestiniationAnimal = (board.grid[move.rowDestination][move.columnDestination].piece?.animal)?.rawValue ?? -1
        
        // The animals are not empty
        guard pieceOriginAnimal > -1 else {return false }
        guard pieceDestiniationAnimal > -1 else {return true }
        
        // elephant cannot eat the rat
        if pieceOriginAnimal == Animal.elephant.rawValue  && pieceDestiniationAnimal == Animal.rat.rawValue {
            return false
        }
        
        // rat can eat the elephant
        if pieceDestiniationAnimal == Animal.elephant.rawValue  && pieceOriginAnimal == Animal.rat.rawValue {
            return true
        }
        
        // the movement respect the jungle food chain
        guard pieceOriginAnimal > pieceDestiniationAnimal else {
            return false
        }
        
        return true
    }
    
    public func isGameOver(fromBoard board: Board, atRow row: Int, andColumn column: Int) -> (isWinner : Bool , theResult : Result) {
        let lastPlace = board.grid[row][column]
        guard let lastPiece = board.grid[row][column].piece else {
            return (false, .notFinished)
        }
        
        guard lastPiece.owner != .noOne else {
            return (false, .notFinished)
        }
        
        // last mouvment place was a den and it is the den of the opposit adversaire => win
        if lastPlace.cellType == .den && lastPlace.initialOwner != lastPiece.owner {
            return (true,  .winner(lastPiece.owner, .denReached) )
        }
        
        // no more pieces left for the adversaire => win
        if (board.grid.flatMap{$0.filter{ $0.piece?.owner == (!lastPiece.owner) }}).isEmpty {
            return (true,  .winner(lastPiece.owner, .noMorePieces) )
        }
        
        // no more moves left for the adversaire => win
        if getMoves(fromBoard: board, ofTheOwner: (!lastPiece.owner)).isEmpty {
            return (true,  .winner(lastPiece.owner, .noMovesLeft) )
        }
        
        // game did not finish
        return (false, .notFinished)
        
    }
    
    public mutating func playedMove(_ move: Move, currentBoard: Board, newBoard: Board) {
        historic.append(move)
    }
    
    
}
