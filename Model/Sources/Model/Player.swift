import Foundation

///This class represent a player in ShouDouQi **You should not use this class use it's subclass**
public class Player {
    public let id : Owner
    public let name : String
    
    public init?(withName Name : String, andId Id : Owner){
        guard Id != .noOne else { return nil }
        name = Name
        id = Id
    }
    
    public func chooseMove(in board: Board, with rules: Rules) -> Move? {
        fatalError("Subclasses need to implement the `chooseMove()` method.")
    }
}
