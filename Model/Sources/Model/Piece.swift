import Foundation

///This struct represent a piece of a player in ShouDouQi
///So it is represented by the owner and the animal of the piece
public struct Piece :  CustomStringConvertible,Equatable,Hashable{
    public let owner : Owner
    public let animal : Animal
    
    public init(withOwner owner: Owner,andAnimal animal: Animal) {
        self.owner = owner
        self.animal = animal
    }
    
    public var description: String { "[\(owner):\(animal)]" }
}
