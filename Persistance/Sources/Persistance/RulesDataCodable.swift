import Foundation
import Model

extension RulesData : Codable{
    enum CodingKeys: String, CodingKey {
        case occurence
        case historic
        case rulesType
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(occurence, forKey: .occurence)
        try container.encode(historic, forKey: .historic)
        try container.encode(rulesType, forKey: .rulesType)
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let occurence = try container.decode([Board: Int].self, forKey: .occurence)
        let historic = try container.decode([Move].self, forKey: .historic)
        let rulesType = try container.decode(String.self, forKey: .rulesType)
        self.init(occurence: occurence, historic: historic, rulesType: rulesType)
    }
}
