import Foundation
import Model

extension PlayerData{
    func ToPlayer(withConfiguration conf : GameConf)-> Player?{
        switch playerType{
        case "\(RandomPlayer.self)":
            return RandomPlayer(withName: name, andId: id)
        case "\(IAPlayer.self)":
            return IAPlayer(withName: name, andId: id)
        case "\(HumanPlayer.self)":
            return HumanPlayer(withName: name, andId: id,andInputMethod: conf.input)
            
        default:
            return RandomPlayer(withName: name, andId: id)
        }
        
    }
}

extension Player{
    func ToPlayerData()-> PlayerData{
        let mirror = Mirror(reflecting: self)
        return PlayerData(id: id, name: name, playerType: "\(mirror.subjectType)")
    }
}
