import Foundation
import Model

extension RulesData{
    func ToRules()-> Rules{
        switch rulesType{
        case "\(VerySimpleRules.self)":
            return VerySimpleRules(occurence: occurence, historic: historic)
        default:
            return VerySimpleRules(occurence: occurence, historic: historic)
        }
        
    }
}

extension Rules{
    func ToRulesData()-> RulesData{
        let mirror = Mirror(reflecting: self)
        return RulesData(occurence: occurence, historic: historic, rulesType: "\(mirror.subjectType)")
    }
}
