import Foundation
import Model

extension CellType : Codable{
    private static let CellTypeStrings : [(encodeProperty : String, cellType : CellType)] = [
        ("0",.den),
        ("1",.jungle),
        ("2",.trap),
        ("3",.unknown),
        ("4",.water),
    ]
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(CellType.CellTypeStrings.first{$0.cellType == self}?.encodeProperty)
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let stringValue = try container.decode(String.self)
        self = CellType.CellTypeStrings.first{$0.encodeProperty == stringValue}?.cellType ?? .unknown
    }
}

