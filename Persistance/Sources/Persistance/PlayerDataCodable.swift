import Foundation
import Model

extension PlayerData : Codable{
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case playerType
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(name, forKey: .name)
        try container.encode(playerType, forKey: .playerType)
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let id = try container.decode(Owner.self, forKey: .id)
        let name = try container.decode(String.self, forKey: .name)
        let playerType = try container.decode(String.self, forKey: .playerType)
        self.init(id: id, name: name, playerType: playerType)
    }
}
