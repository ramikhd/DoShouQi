import Foundation
import Model

public struct RulesData{
    var occurence : [Board: Int]
    var historic : [Move]
    var rulesType : String
    
    init(occurence: [Board : Int], historic: [Move], rulesType: String) {
        self.occurence = occurence
        self.historic = historic
        self.rulesType = rulesType
    }
}
