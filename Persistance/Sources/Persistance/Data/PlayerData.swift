import Foundation
import Model

public struct PlayerData{
    public let id : Owner
    public let name : String
    var playerType : String
    
    init(id: Owner, name: String, playerType: String) {
        self.id = id
        self.name = name
        self.playerType = playerType
    }
    
    
    func parseMove(from input: String,withHumanPlayer humanPlayer : HumanPlayer) -> Move? {
           let components = input.split(separator: " ").compactMap { Int($0) }
           
           guard components.count == 4 else {
               return nil
           }
           
        return Move(owner: humanPlayer.id, rowOrigin: components[0],
                       columnOrigin: components[1],
                       rowDestination: components[2],
                       columnDestination: components[3])
    }
}
