import Foundation
import Model

extension Piece : Codable{
    enum CodingKeys: String, CodingKey {
        case owner
        case animal
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(animal, forKey: .animal)
        try container.encode(owner, forKey: .owner)
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let animal = try container.decode(Animal.self, forKey: .animal)
        let owner = try container.decode(Owner.self, forKey: .owner)
        self.init(withOwner: owner, andAnimal: animal)
    }
}
