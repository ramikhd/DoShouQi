import Foundation
import Model

extension Owner : Codable{
    private static let OwnerStrings : [(encodeProperty : String, owner : Owner)] = [
        ("0",.noOne),
        ("1",.player1),
        ("2",.player2)

    ]
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(Owner.OwnerStrings.first{$0.owner == self}?.encodeProperty)
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let stringValue = try container.decode(String.self)
        self = Owner.OwnerStrings.first{$0.encodeProperty == stringValue}?.owner ?? .noOne
    }
}
