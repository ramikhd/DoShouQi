import Foundation
import Model

public struct GameConf{
    public let input: (HumanPlayer) -> Move?
    
    public init(withInput input: @escaping (HumanPlayer) -> Move?) {
        self.input = input
    }
}
