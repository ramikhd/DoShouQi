import Foundation
import Model

extension Game : Encodable,DecodableWithConfiguration{
    public typealias DecodingConfiguration = GameConf
    
    enum CodingKeys: String, CodingKey {
        case board
        case player1
        case player2
        case rule
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(rule.ToRulesData(), forKey: .rule)
        try container.encode(player1.ToPlayerData(), forKey: .player1)
        try container.encode(player2.ToPlayerData(), forKey: .player2)
        try container.encode(board, forKey: .board)
    }
    
    public init(from decoder: Decoder, configuration conf: GameConf) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let rules = try container.decode(RulesData.self, forKey: .rule).ToRules()
        let player1 = try container.decode(PlayerData.self, forKey: .player1).ToPlayer(withConfiguration: conf)
        let player2 = try container.decode(PlayerData.self, forKey: .player2).ToPlayer(withConfiguration: conf)
        let board =  try container.decode(Board.self, forKey: .board)
        try self.init(withRules: rules, andPlayer1: player1!, andPlayer2: player2!)
        self.board = board
    }
}
